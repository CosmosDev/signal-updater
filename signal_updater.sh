#!/bin/bash

RELEASES_URL="https://github.com/signalapp/Signal-Desktop/releases.atom"
BINARY_URL="https://updates.signal.org/desktop/apt/pool/main/s/signal-desktop/signal-desktop_VERSION_amd64.deb"

TMP_FOLDER="tmp_signal"

SIGNAL_FOLDER="Signal"
SIGNAL_DESKTOP_ENTRY="signal-desktop.desktop"

FILENAME="signal.deb"

version=""
force=false

parse_xml () {
    local IFS=\>
    read -d \< TAG VALUE
}

show_help () {
    echo "Signal Desktop updater for RPM-based distributions"
    echo ""
    echo "Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-f] [-v VERSION]"
    echo ""
    echo "Options:"
    echo "-h            Print help"
    echo "-f            Force update to latest version without asking for confirmation"
    echo "-v            Specify custom version to download"
    exit 0
}

clean_files () {
    [ ${PWD##*/} = $TMP_FOLDER ] && cd ..
    [ -d $TMP_FOLDER ] && rm -rf $TMP_FOLDER
}

check_dependencies () {
    if ! [ -x "$(command -v alien)" ]; then
        echo "Error: alien is required but not installed"
        exit 1
    fi
}

check_already_installed () {
    if [ -d "/opt/$SIGNAL_FOLDER" ]; then
        local proceed
        read -p "Signal is already installed. Proceed? [y/n] " proceed
        [ $proceed != 'y' ] && exit 0
    fi
}

download_tags () {
    local releases=()

    echo "Retrieving release tags..."

    tags=$(curl -s "$RELEASES_URL")

    [ $? -ne 0 ] && { echo "An error occurred while downloading tags"; exit 1; }

    # Retrieve latest releases
    while parse_xml; do
        # Exclude beta versions
        if [[ $TAG = "title" ]] && [[ $VALUE = v* ]] && [[ $VALUE != *beta* ]]; then
            releases+=($(echo $VALUE | tr -d "v"))
        fi
    done <<< $tags

    # Sort releases in descending order
    releases=($(echo ${releases[*]}| tr " " "\n" | sort -rn))
    version=${releases[0]}

    echo "Latest release: $version"
}

download_release () {
    local url=$(echo $BINARY_URL | sed "s/VERSION/$version/g")

    wget -O $FILENAME $url -q --show-progress

    [ $? -ne 0 ] && { echo "An error occurred while downloading the package"; clean_files; exit 1; }
}

install_package () {
    # Convert .deb package
    echo "Converting package..."
    local alien_result=$(alien -rg $FILENAME 2> /dev/null)

    local generated_folder=$(echo $alien_result | sed -r 's/Directory (.*?) prepared./\1/')

    if [ -z "$generated_folder" ] || [ ! -d $generated_folder ]; then
        echo "Something went wrong"
        clean_files
        exit 1
    fi

    # Move files to /opt
    sudo rsync -a -I $generated_folder/opt/$SIGNAL_FOLDER/ /opt/$SIGNAL_FOLDER/

    [ $? -ne 0 ] && { echo "An error occurred while moving files"; clean_files; exit 1; }

    # Update Desktop entry
    mv -f $generated_folder/usr/share/applications/$SIGNAL_DESKTOP_ENTRY ~/.local/share/applications
}

clear

check_dependencies

while getopts hfv: flag; do
    case "${flag}" in
        h) show_help;;
        f) force=true;;
        v) version=${OPTARG};;
    esac
done

# Check if Signal is already installed
[ $force = false ] && check_already_installed

# Download Github tags
[ -z "$version" ] && download_tags

if [ $force == false ]; then
    read -p "Version $version will be installed. Proceed? [y/n] " proceed
    [ $proceed != 'y' ] && exit 0
fi

# Create temporary folder
clean_files
mkdir -p $TMP_FOLDER
cd $TMP_FOLDER

# Download .deb package
download_release

# Install package
install_package

echo "Signal Desktop $version has been installed successfully!"

clean_files
exit 0
