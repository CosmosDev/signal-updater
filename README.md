# signal-updater

Signal Desktop updater for RPM-based distributions.

This script relies on [`alien`](https://sourceforge.net/projects/alien-pkg-convert/) to convert the official `deb` package provided by Signal. Last tested on Fedora 34.

## Dependencies

- alien

### Fedora/RHEL
```sh
sudo dnf install alien
```

## Usage

```sh
sh signal_updater.sh [-h] [-f] [-v VERSION]
```

### Options

| Option | Description | Required |
| ------ | ----------- | -------- |
| -h     | Print help  | No       |
| -f     | Force update to latest version without asking for confirmation | No       |
| -v VERSION | Specify custom version to download | No       |